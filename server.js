/*jshint node:true */
'use strict';

var io     = require('socket.io').listen(9090);
var aesjs  = require('aes-js');
var md5    = require('md5');

var strKey = 'goodluckgoodluck';

// Отключаем вывод полного лога - пригодится в production'е
io.set('log level', 1);

io.sockets.on('connection', function (socket) {
  // Т.к. чат простой - в качестве ников пока используем первые 5 символов от ID сокета
  var ID = (socket.id).toString().substr(0, 5);
  var time = new Date().toLocaleTimeString();
  // Посылаем клиенту сообщение о том, что он успешно подключился и его имя
  socket.json.send({'event': 'connected', 'name': ID, 'time': time, 'key': md5(strKey)});
  // Посылаем всем остальным пользователям, что подключился новый клиент и его имя
  socket.broadcast.json.send({'event': 'userJoined', 'name': ID, 'time': time});
  // Навешиваем обработчик на входящее сообщение
  socket.on('message', function (crypted) {
    var time = new Date().toLocaleTimeString();

    console.log(crypted);

    // Уведомляем клиента, что его сообщение успешно дошло до сервера
    socket.json.send({'event': 'messageSent', 'name': ID, 'text': crypted, 'time': time});

    // Отсылаем сообщение остальным участникам чата
    socket.broadcast.json.send({'event': 'messageReceived', 'name': ID, 'text': crypted, 'time': time});
  });
  // При отключении клиента - уведомляем остальных
  socket.on('disconnect', function() {
    var time = new Date().toLocaleTimeString();
    io.sockets.json.send({'event': 'userSplit', 'name': ID, 'time': time});
  });

});

